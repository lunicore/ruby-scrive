```ruby
Scrive.config endpoint:                      'https://api-testbed.scrive.com',
              client_credentials_identifier: '265682ndl48d0356_16',
              client_credentials_secret:     'j29fn3sl3ifn48s',
              token_credentials_identifier:  'mfl39fnzke7tkd7s_12',
              token_credentials_secret:      'md83smpr3hdmswq'
```

```ruby
doc = Scrive::Document.from_template(1247)
doc.title = "Keff"
doc.status # => "Preparation"
doc.signatories.last.name = "Alexander"
doc.ready!
doc.status # => "pending"
```

```ruby
doc = Scrive::Document.new(1220)
doc.cancel!
```

Do not initialize a document, proceed by only doing changes and then attempt
to read values. The data will be unreliable

This gem misuses mutability and references by using replace instead of
assignments. If this gem breaks, redo it.
