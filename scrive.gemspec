Gem::Specification.new do |s|
  s.name        = 'scrive'
  s.version     = '0.0.0'
  s.date        = '2013-10-31'
  s.summary     = 'Scrive API'
  s.description = 'Thin wrapper around the Scrive API'
  s.authors     = ['Alexander Ekdahl']
  s.email       = 'alexander@ekdahl.io'
  s.files       = ['lib/scrive.rb']
  s.license     = 'MIT'

  s.add_runtime_dependency 'faraday'
end
