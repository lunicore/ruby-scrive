module Scrive
  def self.define_reader(name, key)
    %Q"
      def #{name}
        body['#{key}']
      end
    "
  end

  def self.define_accessor(name, key)
    %Q"
      def #{name}=(value)
        body['#{key}'] = value
      end
    "
  end

  class Document
    attr_reader :id
    attr_accessor :body

    def initialize(id, body = {})
      @id   = id
      @body = body

      get if body.empty?
    end

    # Used to create new draft based on a given PDF file.
    def self.from_file(file)
      #set body
    end

    # Used to create a new draft based on an existing template.
    def self.from_template(template_id)
      response = Scrive.conn.post("createfromtemplate/#{template_id}")
      body     = JSON.parse(response.body)

      new(body['id'], body)
    end

    def change_file
    end

    # Alter some values of a draft.
    def update
      # The recommended approach is to send back a modified body
      request(:post, 'update', @body)
      self
    end
    alias_method :save, :update

    # Makes a document available for signing. If delivery is set to
    # email, invitation mails will be sent to signatories.
    #
    # There is a set of requirements that need to be satisfied before
    # a document can be made available for signing. Currently, the
    # requirements are:
    #
    # * A PDF has to be attached to the document as a main file. This
    #   should be the case if the document was created from file, or
    #   created from a template that had a file attached.
    # * If delivery method for signatory is set to email, it must have
    #   an email field with a proper value.
    # * If delivery method for signatory is set to mobile, it must have
    #   an mobile field with a proper value.
    # * If delivery method for signatory is set to email_mobile, it must
    #   have email and mobile fields with proper values.
    def ready!
      request(:post, 'ready')
    end

    def cancel!
      request(:post, 'cancel')
    end

    # Check current state of document.
    def get
      request(:get, 'get')
    end
    alias_method :reload, :get

    def self.list
      # takes search params returns documents
    end

    # Deletes a document.
    def delete!
      request(:delete, 'delete')
    end

    # Send out reminder emails to signatories of a pending document.
    def remind!
      request(:post, 'remind')
    end

    def download(file = nil)
      if file
        # download specific file
      else
        # download main file
      end
    end

    class Signatory
      def initialize(master, index)
        @master = master
        @index  = index
      end

      def body
        @master.body['signatories'][@index]
      end

      FIELDS = {
        company:        'company',
        company_number: 'companynr',
        email:          'email',
        firstname:      'fstname',
        lastname:       'sndname',
        mobile:         'mobile',
        ssn:            'sigpersnr'
      }

      FIELDS.each do |name, key|
        define_method(name) do
          field = body['fields'].detect { |f| f['name'] == key }
          field['value']
        end

        define_method("#{name}=") do |value|
          field = body['fields'].detect { |f| f['name'] == key }
          field['value'] = value
        end
      end

      AUXILIARY = {
        sign_success_redirect: 'signsuccessredirect',
        authentication:        'authentication',
        reject_redirect:       'rejectredirect'
      }

      AUXILIARY.each do |name, key|
        class_eval(Scrive::define_reader(name, key))
        class_eval(Scrive::define_accessor(name, key))
      end

      def link
        Scrive.endpoint + body['signlink']
      end
    end

    def signatories
      @body['signatories'].map.with_index do |_, index|
        Signatory.new(self, index)
      end
    end

    ACCESSORS = {
      authentication:     'authentication',
      days_to_sign:       'daystosign',
      delivery:           'delivery',
      invitation_message: 'invitationmessage',
      language:           'lang',
      title:              'title',
      apicallbackurl:     'callback'
    }

    READERS = {
      version:  'objectversion',
      status:   'status',
      template: 'template'
    }

    ACCESSORS.each do |name, key|
      class_eval(Scrive::define_reader(name, key))
      class_eval(Scrive::define_accessor(name, key))
    end

    READERS.each do |name, key|
      class_eval(Scrive::define_reader(name, key))
    end

    private

    def request(method, path, body = nil)
      # TODO: handle object version and associated error
      # TODO: handle any payload such as pdf or postdata
      json = JSON.parse(Scrive.conn.send(method, "#{path}/#{@id}") do |req|
        unless body.nil?
          req.body = { json: body.to_json }
        end
      end.body)

      @body.replace json
      #  handle errors such as updating a document in process
      #  handle document does not exist
    end
  end
end
