require 'faraday'
require 'json'

require 'scrive/document'

module Scrive
  class << self
    def config(options = {})
      @@config = options
    end

    def authorization
      "oauth_signature_method=\"PLAINTEXT\",oauth_consumer_key=\"#{@@config[:client_credentials_identifier]}\",oauth_token=\"#{@@config[:token_credentials_identifier]}\",oauth_signature=\"#{@@config[:client_credentials_secret]}&#{@@config[:token_credentials_secret]}\""
    end

    def endpoint
      @@config[:endpoint]
    end

    def conn
      @@conn ||= ::Faraday.new(url: "#{endpoint}/api/v1") do |faraday|
        faraday.request :multipart
        faraday.request :url_encoded
        faraday.headers['authorization'] = authorization
        faraday.adapter Faraday.default_adapter
      end
    end
  end
end
